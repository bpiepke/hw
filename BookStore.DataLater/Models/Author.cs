﻿using System.Collections.Generic;


namespace HWBookStore.DataLayer.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int YearOfBorn { get; set; }
}
}

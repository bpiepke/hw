﻿using HWBookStore.DataLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace HWBookStore.DataLayer
{
    public class BookStoreDbContext : DbContext
    {
        private const string _connectionString = "Data Source=DESKTOP-2S8U1AC;Initial Catalog=BookStoreDb;Integrated Security=True;";

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }
    }
}

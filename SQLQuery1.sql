use BookStoreDB

INSERT INTO [Authors] ([Name], [LastName], [YearOfBorn]) VALUES
('Jan', 'Kowalski', 1912),
('Donald', 'Kaczynski', 1945),
('Andrzej', 'Nowak', 1933),
('Maria', 'Konopnicka', 1888),
('Janusz', 'Nosacz', 1966);

GO

INSERT INTO [Books] ([Title], [YearOfPublication], [Price], [AuthorId], [SoldUnits]) VALUES
('Poradnik emeryta', 2010, 22.12, 1, 5),
('W pustyni i w puszczy', 1922, 39.99, 2, 3),
('Poradnik wedkarza', 2019, 29.99, 3, 0),
('Podarnik kucharza', 1993, 19.99, 4, 0),
('Sprzatanie dla opornych', 1988, 14.99, 1, 2),
('Historia podboju kosmosu', 2019, 9.99, 5, 0);
﻿using System;
using System.Collections.Generic;
using System.Text;
using HWBookStore.DataLayer;

namespace HWBookStore.BuisnessLayer
{
    public class DatabaseInitializer
    {
        public void Initialize()
        {
            using (var ctx = new BookStoreDbContext())
            {
                ctx.Database.EnsureCreated();
            }
        }
    }
}

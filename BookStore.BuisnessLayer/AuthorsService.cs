﻿using HWBookStore.DataLayer.Models;
using System.Collections.Generic;
using HWBookStore.DataLayer;
using System.Linq;
using System;

namespace HWBookStore.BuisnessLayer
{
    public class AuthorsService
    { 
        public Author AddAuthor(Author author)
        {
            using (var context = new BookStoreDbContext())
            {
                if (context.Authors.FirstOrDefault(l => l.LastName == author.LastName) != null)
                {
                    throw new Exception($"The author {author.Name} {author.LastName} already exists.");
                }

                var addEntity = context.Authors.Add(author);

                context.SaveChanges();

                return addEntity.Entity;
            }
        }

        public List<string> GetAllAuthor()
        {
            using (var context = new BookStoreDbContext())
            {
                var allAuthors = context.Authors
                    .Select(author => $"ID: {author.Id} Name: {author.Name} {author.LastName}")
                    .Distinct()
                    .ToList();
                return allAuthors;
            }
        }

        public Author GetAuthorById(int authorId)
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Authors.FirstOrDefault(l => l.Id == authorId);
            }
        }

    }
    
}

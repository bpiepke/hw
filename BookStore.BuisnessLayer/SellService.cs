﻿using System;
using System.Collections.Generic;
using System.Text;
using HWBookStore.DataLayer;
using System.Linq;
using HWBookStore.DataLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace HWBookStore.BuisnessLayer
{
    public class SellService
    {
        private BookService _bookService;
        private AuthorsService _authorsService;

        public SellService()
        {
            _bookService = new BookService();
            _authorsService = new AuthorsService();
        }

        public List<string> GetAllSoldBooks()
        {
            using (var context = new BookStoreDbContext())
            {
                var allbooks = from p in context.Books
                           where p.SoldUnits > 0
                           select p;
                var soldbooks = allbooks
                        .Select(book => $"Name: {book.Author.Name} {book.Author.LastName} Title: {book.Title}"
                        + $" Sold units: {book.SoldUnits} Total income: " + String.Format("{0:N2}", book.SoldUnits * book.Price))
                        .Distinct()
                        .ToList();
                return soldbooks;

            }
        }
        public List<Book> SoldRaportList()
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Books
                    .ToList();
            }
        }
    }
}

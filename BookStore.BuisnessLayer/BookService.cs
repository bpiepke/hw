﻿using System;
using System.Collections.Generic;
using System.Text;
using HWBookStore.DataLayer;
using System.Linq;
using HWBookStore.DataLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace HWBookStore.BuisnessLayer
{
    public class BookService
    {
        private AuthorsService _authorService;

        public BookService()
        {
            _authorService = new AuthorsService();
        }
        
        public int AddBook(Book book)
        {
            int Id;
            using (var context = new BookStoreDbContext())
            {
                var newBookEntry = context.Books.Add(book);
                var addedBookEntry = context.Books.Add(book);
                context.SaveChanges();

                Id = addedBookEntry.Entity.Id;
            }
            return Id;
        }

        public List<string> ListAllBooks()
        {
            using (var context = new BookStoreDbContext())
            {
                {
                    var allBooks = context.Books
                        .Select(book => $"ID: {book.Id}  Name: {book.Author.Name} {book.Author.LastName}"
                        + $"  Title: {book.Title}  Year of Public: {book.YearOfPublication}  Price: {book.Price}\n\n")
                        .Distinct()
                        .ToList();
                    return allBooks;
                }
            }
        }

        public Book GetBookByID(int bookID)
        {
            using (var context = new BookStoreDbContext())
            {

                var book = context.Books.FirstOrDefault(book => book.Id == bookID);
                { 
                    return book;
                }

            }
        }

        public void Update(Book book)
        {
            using (var context = new BookStoreDbContext())
            {
                context.Update(book);
                context.SaveChanges();
            }
        }

        public List<Book> GetBookByAuthorId(int authorID)
        {
            using (var context = new BookStoreDbContext())
            {
                    return context.Books
                    .Include(l => l.Author)
                    .Where(l => l.AuthorId == authorID)
                    .ToList();  
            }
        }
    }
}
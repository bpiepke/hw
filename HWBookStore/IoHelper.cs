﻿using System;

namespace HWBookStore
{
    internal class IoHelper
    {
        public int GetIntFromUser(string message)
        {
            var result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = int.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine($"'{input}' is not a number. Please try again.");
                }
            }
            return result;
        }


        public string GetStringFromUser(string message)
        {
            Console.Write($"{message}: ");
            return Console.ReadLine();
        }

        public void PressKeyToContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public double GetDoubleFromUser(string message)
        {
            double result = 0;
            var success = false;

            while (!success)
            {
                Console.WriteLine(message);
                var input = Console.ReadLine();
                success = double.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("Incorrect input format");
                }
            }
            return result;
        }
    }
}

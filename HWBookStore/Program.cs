﻿using System;
using HWBookStore.BuisnessLayer;
using System.Linq;
using HWBookStore.DataLayer.Models;

namespace HWBookStore.Cli
{
    public class Program
    {
        private IoHelper _ioHelper;
        private AuthorsService _authorService;
        private BookService _bookService;
        private SellService _sellService;
        private DatabaseInitializer _databaseInitializer;

        public Program()
        {
            _ioHelper = new IoHelper();
            _authorService = new AuthorsService();
            _sellService = new SellService();
            _bookService = new BookService();
            _databaseInitializer = new DatabaseInitializer();
        }

        static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            InitializeDatabase();
            DisplayMenu();
        }
        private void InitializeDatabase()
        {
            _databaseInitializer.Initialize();
        }
        private void DisplayMenu()
        {
            while (true)
            {
                Console.Clear();

                Console.WriteLine("Choose the option below.\n");
                Console.WriteLine("1. Add author");
                Console.WriteLine("2. Add book");
                Console.WriteLine("3. Print all books");
                Console.WriteLine("4. Print all books by author last name");
                Console.WriteLine("5. Sell book");
                Console.WriteLine("6. Print sold books");
                Console.WriteLine("7. Show book by sold");
                Console.WriteLine("8. Exit\n");
                var input = _ioHelper.GetIntFromUser("Enter the number");

                Console.Clear();

                switch (input)
                {
                    case 1:
                        AddAuthor();
                        break;
                    case 2:
                        AddBook();
                        break;
                    case 3:
                        GetAllBooks();
                        break;
                    case 4:
                        ShowBookByAuthor();
                        break;
                    case 5:
                        SellBooks();
                        break;
                    case 6:
                        SoldBooks();
                        break;
                    case 7:
                        ShowBookBySold();
                        break;
                    case 8:
                        return;
                    default:
                        Console.WriteLine($"Number {input} is invalid. Please try again.");
                        _ioHelper.PressKeyToContinue();
                        break;
                };
            }
        }
        private void AddAuthor()
        {
            Console.WriteLine("You want to add a new author. Please enter the data below.\n");
            Author newAuthor = new Author()
            {
                Name = _ioHelper.GetStringFromUser("Name"),
                LastName = _ioHelper.GetStringFromUser("Surname"),
                YearOfBorn = _ioHelper.GetIntFromUser("Year of born")
            };
            _authorService.AddAuthor(newAuthor);

            Console.WriteLine("Author Added");

            _ioHelper.PressKeyToContinue();
        }
        private void AddBook()
        {
            foreach(var author in _authorService.GetAllAuthor())
            {
                Console.WriteLine(author);
            }

            var authorId = _ioHelper.GetIntFromUser("\nChoose author ID");
            var authors = _authorService.GetAuthorById(authorId);

            Author inputID = _authorService.GetAuthorById(authorId);
            if (inputID == null)
            {
                Console.WriteLine($"Author with ID {authorId} doesnt exist.");
                _ioHelper.PressKeyToContinue();
                return;
            }
            Console.WriteLine($"You choose ID: {authors.Id} {authors.Name} {authors.LastName}");

            Book newBook = new Book
            {
                AuthorId = authorId,
                Title = _ioHelper.GetStringFromUser("Enter the title"),
                YearOfPublication = _ioHelper.GetIntFromUser("Enter the year of publication"),
                Price = _ioHelper.GetDoubleFromUser("Enter the price")
            };
            _bookService.AddBook(newBook);
            Console.WriteLine("Book added");
            _ioHelper.PressKeyToContinue();
        }
        private void GetAllBooks()
        {
            foreach (var book in _bookService.ListAllBooks())
            {
                Console.WriteLine(book);
            }
            _ioHelper.PressKeyToContinue();

        }
        private void ShowBookByAuthor()
        {
            foreach (var author in _authorService.GetAllAuthor())
            {
                Console.WriteLine(author);
            }

            var authorID = _ioHelper.GetIntFromUser("Choose the Author ID: ");

            var books = _bookService.GetBookByAuthorId(authorID);
            Console.WriteLine();
            foreach (var book in books)
            {
                    Console.WriteLine($"Name: {book.Author.Name} {book.Author.LastName} Title: {book.Title}" +
                    $" Year Of public: {book.YearOfPublication}  Price: {book.Price}\n\n");
            }

            _ioHelper.PressKeyToContinue();
        }
        private void SellBooks()
        {
            foreach(var books in _bookService.ListAllBooks())
            {
                Console.WriteLine(books);
            }

            var bookID = _ioHelper.GetIntFromUser("Write the Book ID");
            var book = _bookService.GetBookByID(bookID);

            if(book == null)
            {
                Console.WriteLine($"No book with ID: {bookID}");
                _ioHelper.PressKeyToContinue();
                return;
            }
            else
            {
                book.SoldUnits += 1;

                _bookService.Update(book);
                Console.WriteLine("Sold! Good choice!");
                _ioHelper.PressKeyToContinue();
            }

        }
        private void SoldBooks()
        {
            foreach (var soldbooks in _sellService.GetAllSoldBooks())
            {
                Console.WriteLine(soldbooks);
                Console.WriteLine();
            }
            _ioHelper.PressKeyToContinue();
        }
        private void ShowBookBySold()
        {
            var allBooks = _sellService.SoldRaportList().OrderByDescending(x => x.SoldUnits);

            var sold = allBooks.Where(x => x.SoldUnits > 0);
            var notSold = allBooks.Where(x => x.SoldUnits == 0);

            foreach (var book in sold)
            {
                Console.WriteLine($"Title: {book.Title} Year: { book.YearOfPublication} Sold units: {book.SoldUnits} Total income: " 
                    + String.Format("{0:N2}", book.SoldUnits * book.Price), "\n");
                Console.WriteLine();
            }
            Console.WriteLine("______________________");
            Console.WriteLine();
            foreach (var book in notSold)
            {
                Console.WriteLine($"Title: {book.Title} Year: { book.YearOfPublication} Sold units: {book.SoldUnits} Total income: "
                       + String.Format("{0:N2}", book.SoldUnits * book.Price),"\n");
                Console.WriteLine();
            }
            _ioHelper.PressKeyToContinue();
        }

    }
}
